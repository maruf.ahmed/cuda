/********************************************
Matrix multiplication in CUDA
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define MATRIXSIZE 20 
#define BLOCKSIZE  32


 __global__ 
 void matrixMultiplication(int N, int *a, int *b, int *c){
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    int cell_sum = 0;

    for (int k =0; k < N; k ++){
        if ( row < N && col < N)
           cell_sum += a[row*N + k] * b[ k * N + col]; 
    }
    if (row < N && col < N )
       c[row*N + col] = cell_sum;
}


extern __global__ void matrixMultiplication(int N, int *a, int *b, int *c);

int main(){
    
    int * h_a, * h_b, * h_c;
    int * d_a, * d_b, * d_c;

 
    /** Allocate Memory for the host  **/
    h_a = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));
    h_b = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));
    h_c = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));

    for(int i=0; i < MATRIXSIZE*MATRIXSIZE; i++ ){
        h_a [i] = rand() % MATRIXSIZE;
        h_b [i] = rand() % MATRIXSIZE;  
    } 

   printf("Matrix A:\n");
   for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_a[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }  
   printf("Matrix B:\n");
   for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_b[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }  
   
    cudaMalloc((void **) & d_a , (MATRIXSIZE * MATRIXSIZE )* sizeof(int));
    cudaMalloc((void **) & d_b , (MATRIXSIZE * MATRIXSIZE )* sizeof(int));
    cudaMalloc((void **) & d_c , (MATRIXSIZE * MATRIXSIZE )* sizeof(int));

    cudaMemcpy(d_a, h_a, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyHostToDevice );
    cudaMemcpy(d_b, h_b, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyHostToDevice );

    int const GRIDSIZE = ((MATRIXSIZE + BLOCKSIZE - 1)/BLOCKSIZE);

    dim3 BLOCKS (BLOCKSIZE, BLOCKSIZE);
    dim3 GRIDS  (GRIDSIZE, GRIDSIZE);

    matrixMultiplication<<<GRIDS, BLOCKS>>>(MATRIXSIZE, d_a, d_b, d_c);

    cudaMemcpy(h_c, d_c, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyDeviceToHost);
    printf("Resulted Matrix (AxB):\n");
    for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_c[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }
    
    return 0;
}
