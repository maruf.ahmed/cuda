/********************************************
Histogram computation in Cuda with shared memory  
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>

#define BLOCKSIZE   (32)
#define GRIDSIZE    (64)
#define ARRAYSIZE   (256*256)
#define H_SIZE      (20)

__global__ void histogramSharedMemory (int N, int * a, int * b){
     
    int globalThreadIdx =  blockIdx.x * blockDim.x +  threadIdx.x;

    __shared__ int sharedHistogram [H_SIZE];

    if (threadIdx.x < H_SIZE )
         sharedHistogram [threadIdx.x] = 0;

     __syncthreads();    
   
    for(int i = globalThreadIdx; i < N; i += blockDim.x * gridDim.x  ) 
        
         atomicAdd ( &sharedHistogram[ a [i] ], 1);
    
    __syncthreads(); 

    if (threadIdx.x < H_SIZE )
        atomicAdd ( &b[threadIdx.x] ,  sharedHistogram [threadIdx.x] );

}


int main(){
    
    int * h_a , *h_b ;
    int * d_a , * d_b;
    cudaError_t cudaStat;
    
    float time;
    cudaEvent_t start, stop;

    printf("\nVector size: %d, Block size: %d, grid size: %d \n", ARRAYSIZE, BLOCKSIZE, GRIDSIZE);

    h_a = (int*) malloc ( ARRAYSIZE * sizeof(int));
    h_b = (int*) malloc ( H_SIZE    * sizeof(int));

    for (int i=0; i<ARRAYSIZE; i++)
       h_a[i] = rand()% H_SIZE;

    printf("\nVector A: \n");
    for (int i=0; i< ARRAYSIZE; i ++)
        printf("%d ", h_a[i]);
    printf("\n");    
   
    cudaStat = cudaMalloc ( (void **) & d_a,  ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_b, H_SIZE  * sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       return EXIT_FAILURE;
    }

    cudaMemcpy ( d_a , h_a, ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);

    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    histogramSharedMemory <<<GRIDSIZE, BLOCKSIZE >>>(ARRAYSIZE, d_a, d_b );
    cudaStat  = cudaGetLastError();
    if (cudaStat != cudaSuccess){
         printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
         free(h_a);
         free(h_b);
         cudaFree(d_a);
         cudaFree(d_b);        
         return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nTime: %.3f ms\n", time);
     
    cudaStat = cudaMemcpy(h_b, d_b,  H_SIZE  * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       cudaFree(d_b);
       return EXIT_FAILURE;
    }

    printf("\nHistogram : [0 to %d]\n",H_SIZE-1);
    for (int i=0; i< H_SIZE; i ++)
         printf("%d ", h_b[i]);
     printf("\n");  
    
    free(h_a);
    free(h_b);
    cudaFree(d_a);
    cudaFree(d_b);
    return 0;
}
