/********************************************
Multiblock Reduction in Cuda 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define ARRAYSIZE 100000
#define BLOCKSIZE 128 
#define GRIDSIZE  32 

__global__ void sumMultiBlock (long N, long * a, long * b){
    long localTreadIdx = threadIdx.x;
    long globalTreadIdx =  blockIdx.x * blockDim.x +  localTreadIdx;
    long gridSize = BLOCKSIZE * gridDim.x;
    long sum = 0; 

    __shared__ long sharedArray[BLOCKSIZE*sizeof(long)]; 
 
    for (long i = globalTreadIdx ; i < N ; i = i + gridSize){
        sum = sum + a[i];
    } 
    sharedArray[localTreadIdx] = sum;
    __syncthreads ();
  
    for(long offset = BLOCKSIZE/2; offset > 0 ; offset /= 2 ){
        if (threadIdx.x < offset)
           sharedArray[localTreadIdx] = sharedArray[localTreadIdx] + sharedArray[localTreadIdx + offset];
        __syncthreads() ;   
    }
      
    if (localTreadIdx == 0)
       b [blockIdx.x]  = sharedArray [ 0 ];
}


int main(){
    
    long * h_a , *h_b , sum=0;
    long * d_a , * d_b;

    h_a = (long*) malloc ( ARRAYSIZE * sizeof(long));
    h_b = (long*) malloc ( GRIDSIZE  * sizeof(long));

    for (long i=0; i<ARRAYSIZE; i++)
       h_a[i] = rand()% ARRAYSIZE;
      
    
    cudaMalloc ( (void **) & d_a, ARRAYSIZE * sizeof(long) );
    cudaMalloc ( (void **) & d_b, GRIDSIZE * sizeof (long) );

    cudaMemcpy ( d_a , h_a, ARRAYSIZE * sizeof(long), cudaMemcpyHostToDevice);
  
    sumMultiBlock <<<GRIDSIZE, BLOCKSIZE>>>(ARRAYSIZE, d_a, d_b );
    cudaDeviceSynchronize();

    cudaMemcpy(h_a, d_a, ARRAYSIZE * sizeof(long), cudaMemcpyDeviceToHost );
    
    printf("Initial Array:\n");
    for(long i=0; i<ARRAYSIZE; i++)
        printf("%ld ", h_a[i]);
    printf("\n");

    cudaMemcpy(h_b, d_b, GRIDSIZE * sizeof(long), cudaMemcpyDeviceToHost );
    /*********************************
    printf("Intermediate Array:\n");
    for(long i=0; i<GRIDSIZE; i++){
        printf("%ld ", h_b[i]);
    }
    printf("\n");
    *********************************/
    sumMultiBlock <<<1, BLOCKSIZE>>>(GRIDSIZE, d_b, d_b);
    cudaDeviceSynchronize();

    cudaMemcpy(&sum, d_b, sizeof(long),cudaMemcpyDeviceToHost);

    free(h_a);
    free(h_b);
    cudaFree(d_a);
    cudaFree(d_b);

    printf("\nSum: %ld\n", sum);
    
    return 0;
}
