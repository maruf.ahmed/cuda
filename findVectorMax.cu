/********************************************
Finding max value in Cuda 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>

#define BLOCKSIZE  (32)
#define GRIDSIZE   (32)
#define ARRAYSIZE  (32*32)

__global__ void findVectorMax (int * a, int * max_a, int *lock){
     
    int globalThreadIdx =  blockIdx.x * blockDim.x +  threadIdx.x;
    int offset, localMax;

    __shared__ int sharedArray[BLOCKSIZE]; 

    offset = 0;
    localMax = -1;
    
    while (globalThreadIdx + offset < ARRAYSIZE){
        localMax = (localMax > a [globalThreadIdx + offset]) ? localMax : a [globalThreadIdx + offset];
        offset +=  gridDim.x * blockDim.x;
    }
   
   sharedArray[threadIdx.x] = localMax; 
 
    __syncthreads ();
       
    offset =  blockDim.x / 2;  
    while (offset > 0 ){
        if (threadIdx.x < offset )
              sharedArray[threadIdx.x ]  
                  =  sharedArray[threadIdx.x ] > sharedArray[threadIdx.x + offset] ?  sharedArray[threadIdx.x ] : sharedArray[threadIdx.x + offset];
        __syncthreads() ;  

        offset = offset/2;
    }
    
    if (threadIdx.x == 0){
       while (atomicCAS (lock,0,1) != 0)
         ; 
       *max_a  = *max_a > sharedArray [ 0 ]  ?  *max_a : sharedArray [ 0 ] ;
       atomicExch (lock, 0);
    }
}


int main(){
    
    int * h_a , *h_b ;
    int * d_a , * d_b; 
    int * d_lock;
    cudaError_t cudaStat;
    float time;
    cudaEvent_t start, stop;

    printf("\nVector size: %d, Block size: %d, grid size: %d \n", ARRAYSIZE, BLOCKSIZE, GRIDSIZE);

    h_a = (int*) malloc ( ARRAYSIZE * sizeof(int));
    h_b = (int*) malloc ( sizeof(int));

    for (int i=0; i<ARRAYSIZE; i++)
       h_a[i] = rand()%  (ARRAYSIZE*2); //(ARRAYSIZE);

    printf("\nVector A: \n");
    for (int i=0; i< ARRAYSIZE; i ++)
        printf("%d ", h_a[i]);
    printf("\n");    
   
    cudaStat = cudaMalloc ( (void **) & d_a,  ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_b, sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_lock, sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       cudaFree(d_b);
       return EXIT_FAILURE;
    }

    cudaMemcpy ( d_a , h_a, ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemset ( d_b , -1, sizeof(int));
    cudaMemset ( d_lock , 0, sizeof(int));
    
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    findVectorMax <<<GRIDSIZE, BLOCKSIZE >>>(d_a, d_b,  d_lock);
    cudaStat  = cudaGetLastError();
    if (cudaStat != cudaSuccess){
            printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
            free(h_a);
            free(h_b);
            cudaFree(d_a);
            cudaFree(d_b);        
            return EXIT_FAILURE;
     }
    cudaDeviceSynchronize();
  
    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel Time: %.3f ms\n", time);
     
    cudaStat = cudaMemcpy(h_b, d_b, sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStat != cudaSuccess){
            printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
            free(h_a);
            free(h_b);
            cudaFree(d_a);
            cudaFree(d_b);        
            return EXIT_FAILURE;
     }
 
    printf("\nMax: %d\n", *h_b); 

    free(h_a);
    free(h_b);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_lock);
    return 0;
}
