/********************************************
Sum Reduction in Cuda with cooperative groups based optimization
Required Cuda 9 or higher to compile
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <cooperative_groups.h>

using namespace cooperative_groups;

#define BLOCKSIZE (32)
#define ARRAYSIZE (256*256)

__device__  int threadSum (int N, int *a ){
     
     int sum = 0;
     int globalThreadIdx = ( blockIdx.x * blockDim.x ) + threadIdx.x;

     for (int offset = globalThreadIdx; offset <= N/4 ; offset += (gridDim.x * blockDim.x) ){
         int4 quad = ( (int4*) a ) [offset];
         sum += quad.x + quad.y + quad.z + quad.w;
     }
     /*********************************************************************************
     for (int offset = globalThreadIdx; offset < N ; offset += gridDim.x * blockDim.x ) 
         sum += a [offset];
     *********************************************************************************/
     return sum;
}


__device__ int reduceSum (thread_group g, int *a, int sum){
    
    int gRank = g.thread_rank();

    for (int offset = g.size()/2 ; offset > 0 ; offset /=2 ){
         a[gRank] = sum;
         g.sync();
         if (gRank < offset)
            sum += a [gRank + offset]; 
         g.sync();
    }
    
    return sum;
}


__global__ void sumReductionWithCoOpGroups (int N, int * a, int * sum_a){
  
    extern 
    __shared__ int sharedArray[];
    int blockSum =0, tSum = 0; 

    tSum = threadSum (N, a );
        
    auto g = this_thread_block ();

    blockSum = reduceSum (g,  sharedArray, tSum );

    if (g.thread_rank() == 0)
          atomicAdd(sum_a, blockSum) ;
}


int main(){
    
    int * h_a , *h_b ;
    int * d_a , * d_b;
    
    float time;
    cudaEvent_t start, stop;
    cudaError_t cudaStat;

    int gridSize = (ARRAYSIZE  + BLOCKSIZE - 1) / BLOCKSIZE; 

    printf("\nVector size: %d, Block size: %d, Grid size: %d \n", ARRAYSIZE, BLOCKSIZE, gridSize);

    h_a = (int*) malloc ( ARRAYSIZE * sizeof(int) );
    h_b = (int*) malloc ( sizeof(int) );

    for (int i=0; i<ARRAYSIZE; i++)
       h_a[i] = rand()% 1000; // ARRAYSIZE;

    printf("\nVector A: \n");
    for (int i=0; i< ARRAYSIZE; i ++)
        printf("%d ", h_a[i]);
    printf("\n");    
   
    cudaStat = cudaMalloc ( (void **) & d_a,  ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_b,  sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       return EXIT_FAILURE;
    }

    cudaMemcpy ( d_a , h_a, ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemset ( d_b , 0,  sizeof(int));
        
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    sumReductionWithCoOpGroups <<<gridSize, BLOCKSIZE, BLOCKSIZE *sizeof(int) >>>(ARRAYSIZE, d_a, d_b );
    cudaStat  = cudaGetLastError();
    if (cudaStat != cudaSuccess){
          printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
          free(h_a);
          free(h_b);
          cudaFree(d_a);
          cudaFree(d_b);        
          return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nTime: %.3f ms\n", time);
     
    cudaStat = cudaMemcpy(h_b, d_b, sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       cudaFree(d_b);
       return EXIT_FAILURE;
    }

    printf("\nSum: %d\n", *h_b); 

    free(h_a);
    free(h_b);
    cudaFree(d_a);
    cudaFree(d_b);
    return 0;
}
