/********************************************
Tiled Matrix multiplication in CUDA
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define MATRIXSIZE (1024*4)
#define BLOCKSIZE  32
#define TILE_WIDTH 32

__global__
void tiledMatrixMultiplication(int M, int K, int N,  int *a, int *b, int *c){
    __shared__ int ds_a[TILE_WIDTH][TILE_WIDTH];
    __shared__ int ds_b[TILE_WIDTH][TILE_WIDTH];

    int row = blockDim.y * blockIdx.y + threadIdx.y ;
    int col = blockDim.x * blockIdx.x + threadIdx.x ;
 
    /***********************************
     a [row][t*TILE_WIDTH + tx]
     a [row*n + t*TILE_WIDTH + tx]
     b [t*TILE_WIDTH + ty][col]
     b [(t * TILE_WIDTH + ty)*k + col] 
    ***********************************/

    int csum = 0; 
    for (int t=0; t< N/TILE_WIDTH; t++){
        ds_a[threadIdx.y][threadIdx.x] = a [row*N + t*TILE_WIDTH + threadIdx.x];
        ds_b[threadIdx.y][threadIdx.x] = b [ (t*TILE_WIDTH + threadIdx.y) * K + col];
        __syncthreads();

        for (int i=0; i < TILE_WIDTH; i++)
           csum += ds_a[threadIdx.y][i] * ds_b[i][threadIdx.x];
        __syncthreads();   
    }
    c[row * N + col] = csum;
}

 __global__ 
 void matrixMultiplication(int N, int *a, int *b, int *c){
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    int cell_sum = 0;

    for (int k =0; k < N; k ++){
        if ( row < N && col < N)
           cell_sum += a[row*N + k] * b[ k * N + col]; 
    }
    if (row < N && col < N )
       c[row*N + col] = cell_sum;
}


/********************************************************************* 
 ************  Cuda Error handling functions  *******************/
void kernelErrorPrint(){
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess){
        printf("Kernel execution Error: %s, at File: %s\n", cudaGetErrorString(err), __FILE__);
        exit(-1);
    }
}

void cudaErrorPrint(cudaError_t cudaERR){
    if (cudaERR != cudaSuccess)
      printf ("Cuda Error : %s,  at File: %s\n", cudaGetErrorString(cudaERR), __FILE__);
}


int main(){
    
    int * h_a, * h_b, * h_c;
    int * d_a, * d_b, * d_c;
    int * result1, * result2;
    int GRIDSIZE;
    float time;
    cudaEvent_t start, stop;
 
    /** Allocate Memory for the host  **/
    h_a = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));
    h_b = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));
    h_c = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));
    result1 = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));
    result2 = ( int *) malloc (MATRIXSIZE * MATRIXSIZE * sizeof (int));

    for(int i=0; i < MATRIXSIZE*MATRIXSIZE; i++ ){
        h_a [i] = rand() % MATRIXSIZE;
        h_b [i] = rand() % MATRIXSIZE;  
    } 
   
   printf("Matrix A:\n");
   printf("Dimension: %d x %d\n", MATRIXSIZE, MATRIXSIZE);
   /***********************************************
   for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_a[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }  
   ***********************************************/

   printf("Matrix B:\n");
   printf("Dimension: %d x %d\n", MATRIXSIZE, MATRIXSIZE);
   /***********************************************
   for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_b[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }
    ***********************************************/  
   
    cudaErrorPrint( cudaMalloc((void **) & d_a , (MATRIXSIZE * MATRIXSIZE )* sizeof(int)) );
    cudaErrorPrint( cudaMalloc((void **) & d_b , (MATRIXSIZE * MATRIXSIZE )* sizeof(int)) );
    cudaErrorPrint( cudaMalloc((void **) & d_c , (MATRIXSIZE * MATRIXSIZE )* sizeof(int)) );

    cudaMemcpy(d_a, h_a, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyHostToDevice );
    cudaMemcpy(d_b, h_b, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyHostToDevice );

    GRIDSIZE = ((MATRIXSIZE + BLOCKSIZE - 1)/BLOCKSIZE);

    dim3 BLOCKS (BLOCKSIZE, BLOCKSIZE);
    dim3 GRIDS  (GRIDSIZE, GRIDSIZE);
   
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    matrixMultiplication<<<GRIDS, BLOCKS>>>(MATRIXSIZE, d_a, d_b, d_c);
    kernelErrorPrint();
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);

    cudaMemcpy(h_c, d_c, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyDeviceToHost);
    cudaMemcpy(result1, d_c, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyDeviceToHost);

    printf("Parallel Matrix multiplication (AxB):\n");
    printf("GRIDSIZE: %d, BLOCKSIZE: %d, time: %.3f ms\n", GRIDSIZE, BLOCKSIZE, time);
    /**************************************************
    for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_c[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }
    **************************************************/

    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    tiledMatrixMultiplication<<<GRIDS, BLOCKS>>>(MATRIXSIZE, MATRIXSIZE, MATRIXSIZE,  d_a, d_b, d_c);
    kernelErrorPrint();
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
   
    cudaMemcpy(h_c, d_c, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyDeviceToHost);
    cudaMemcpy(result2, d_c, MATRIXSIZE * MATRIXSIZE * sizeof (int), cudaMemcpyDeviceToHost);

    printf("Tiled Matrix multiplication (AxB):\n");
    printf("GRIDSIZE: %d, BLOCKSIZE: %d, time: %.3f ms\n", GRIDSIZE, BLOCKSIZE, time);
    /**************************************************
    for(int i = 1; i<=MATRIXSIZE * MATRIXSIZE; i++){
        printf("%6d", h_c[i-1]);
        if ( i != 0 && i % MATRIXSIZE == 0){
            printf("\n");
        }
    }
    **************************************************/

    return 0;
}
