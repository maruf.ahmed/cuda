/******************************************
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h> 

#define ARRAYSIZE 100000



__global__ void addArray (int n, const int * a, const int *b, int *c) {
    

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if ( i < n)
        c[i] = a[i] + b[i];
    
}


int main(){

    time_t t;
    int * h_a, *h_b, *h_c;
    int * d_a, *d_b, *d_c;
    int  blockSize, threadSize;


    h_a = (int*) malloc (ARRAYSIZE * sizeof (int));
    h_b = (int*) malloc (ARRAYSIZE * sizeof (int)); 
    h_c = (int*) malloc (ARRAYSIZE * sizeof (int));

    srand ((unsigned) time (&t));
    for (int i = 0; i < ARRAYSIZE; i++){
        h_a [i] = rand()% ARRAYSIZE;
        h_b [i] = rand()% ARRAYSIZE;
    }
    printf("\nA: ");
    for (int i = 0; i < ARRAYSIZE ; i ++){
        printf("%6d", h_a[i]);
    }
    printf("\nB: ");
    for (int i = 0; i < ARRAYSIZE ; i ++){
        printf("%6d", h_b[i]);
    }
    
    cudaMalloc ((void **) & d_a, ARRAYSIZE * sizeof(int));
    cudaMalloc ((void **) & d_b, ARRAYSIZE * sizeof(int));
    cudaMalloc ((void **) & d_c, ARRAYSIZE * sizeof(int));

    cudaMemcpy (d_a , h_a , ARRAYSIZE * sizeof(int) , cudaMemcpyHostToDevice);
    cudaMemcpy (d_b , h_b , ARRAYSIZE * sizeof(int) , cudaMemcpyHostToDevice);
 
    threadSize = 32;
    blockSize = ((ARRAYSIZE + threadSize - 1) / threadSize); 

    addArray<<< blockSize, threadSize >>> (ARRAYSIZE, d_a, d_b, d_c);

    cudaDeviceSynchronize();

    cudaMemcpy (h_c , d_c, ARRAYSIZE * sizeof (int), cudaMemcpyDeviceToHost);

    printf("\n-----");
    for (int i = 0; i < ARRAYSIZE; i++){
        printf ("------");
    }
    printf("\nC: ");
    for (int i = 0; i < ARRAYSIZE; i++){
        printf ("%6d", h_c[i]);
    }

    cudaFree (d_a);
    cudaFree (d_b);
    cudaFree (d_c); 

    
    free (h_a);
    free (h_b);
    free (h_c);

    return 0;
}
