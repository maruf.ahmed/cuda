/********************************
An example of using Cuda built in blocks and threads number optimization feature.
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
*********************************/

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h> 
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#define ARRAYSIZE 1000000

__global__ void ArrayRandomValues (int N, float *array){
    long i = blockIdx.x * blockDim.x +  threadIdx.x; 

    if ( i < N ){
        curandState crstate;
        curand_init (clock64(), i, 0, &crstate );
        array[i] = curand_uniform (&crstate);
    } 
}

__global__ void multiplyArray (int N, const float * a, const float *b, float *c) {
    
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if ( i < N)
        c[i] = a[i] * b[i];
}


void timeExecution(long int blockSize, long int threadSize, int N, const float * a, const float *b, float *c ){
    float time;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    multiplyArray<<<blockSize, threadSize >>> (N, a, b, c)  ;

    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
    

    printf("\n %8ld %8ld %9.3f   \n", blockSize, threadSize, time);
}

void findOptimize(){
 
    float * h_a, *h_b, *h_c;
    float * d_a, *d_b, *d_c;
    int  blockSize, threadSize;
    int  minGridSize=0;

    h_a = (float*) malloc (ARRAYSIZE * sizeof (float));
    h_b = (float*) malloc (ARRAYSIZE * sizeof (float)); 
    h_c = (float*) malloc (ARRAYSIZE * sizeof (float));

    cudaMalloc ((void **) & d_a, ARRAYSIZE * sizeof(float));
    cudaMalloc ((void **) & d_b, ARRAYSIZE * sizeof(float));
    cudaMalloc ((void **) & d_c, ARRAYSIZE * sizeof(float));

    threadSize = 1024;
    blockSize = ((ARRAYSIZE + threadSize - 1) / threadSize); 

    ArrayRandomValues<<< blockSize, threadSize >>> (ARRAYSIZE, d_a);
    ArrayRandomValues<<< blockSize, threadSize >>> (ARRAYSIZE, d_b);

    cudaMemcpy (h_a , d_a, ARRAYSIZE * sizeof (float), cudaMemcpyDeviceToHost);
    cudaMemcpy (h_b , d_b, ARRAYSIZE * sizeof (float), cudaMemcpyDeviceToHost);
    
    printf("\n %6s  %6s  %6s  \n", "Blocksize", "threadSize", "time (ms)");

    for(long i=2; i<ARRAYSIZE; i=i*2){
       threadSize = i;
       blockSize = ((ARRAYSIZE + threadSize - 1) / threadSize); 
       timeExecution(blockSize, threadSize, ARRAYSIZE,  d_a, d_b, d_c );
    }
    
    printf("\nOptimal Blocks and Threads:\n");
    cudaOccupancyMaxPotentialBlockSize(& minGridSize, & blockSize,  multiplyArray, 0, ARRAYSIZE); 
    timeExecution(blockSize, threadSize, ARRAYSIZE,  d_a, d_b, d_c );

    cudaMemcpy (h_c , d_c, ARRAYSIZE * sizeof (float), cudaMemcpyDeviceToHost);
  
    printf("\nA: ");
    for (int i = 0; i < ARRAYSIZE ; i ++){
        printf("%6.3f", h_a[i]);
    }
    printf("\nB: ");
    for (int i = 0; i < ARRAYSIZE ; i ++){
        printf("%6.3f", h_b[i]);
    }

    printf("\n-----");
    for (int i = 0; i < ARRAYSIZE; i++){
        printf ("------");
    }
    printf("\nC: ");
    for (int i = 0; i < ARRAYSIZE; i++){
        printf ("%6.3f", h_c[i]);
    }

    printf("\nEnd");

    cudaFree (d_a);
    cudaFree (d_b);
    cudaFree (d_c); 
    
    free (h_a);
    free (h_b);
    free (h_c);   
}


int main(){

    findOptimize();
    return 0;
}
