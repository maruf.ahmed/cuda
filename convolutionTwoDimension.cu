/********************************************
Two dimensional matrix colvolution calculation in Cuda
 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <cooperative_groups.h>
#include <stdbool.h>

using namespace cooperative_groups;

#define BLOCKSIZE  (2)
#define ARRAYSIZE  (32)
#define MASKSIZE   (8)



__global__ void convolutionTwoDimension (int N, int M, int * a, int * b, int * c){
  
    int globalCol = blockIdx.x * blockDim.x + threadIdx.x;
    int globalRow = blockIdx.y * blockDim.y + threadIdx.y;

    int convoSum = 0; 
    
    for (int i = 0; i < M ; i++)
       for (int j = 0; j < M ; j++)
          if (globalRow - M/2 + i >= 0 && globalRow - M/2 + i < N  &&
                    globalCol - M/2 + j >= 0 && globalCol - M/2 + j < N )
            convoSum += a [(globalRow - M/2 + i)*N + (globalCol - M/2 + j)  ] * b [i* M + j]; 

    c[globalRow * N + globalCol] = convoSum; 
}

void sequential_convolution (int N, int M, int * a, int * b, int * c){
    int convoSum; 
   
    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++){
             convoSum = 0;
             for (int k = 0; k < M ; k++)
                 for (int l = 0; l < M ; l++)
                      if (i - M/2 + k >= 0 && i - M/2 + k < N  &&
                           j - M/2 + l >= 0 && j - M/2 + l < N ) 
                           convoSum += a [ (i - M/2 + k) * N + (j - M/2 + l) ] * b [k* M + l]; 
            c[i*N + j] = convoSum;
    }
}

bool comapreResults(int N, int *a , int *b){
    
    for (int i=0; i<N; i++)
      if (a[i] != b [i])
         return false;
   return true;       
}

int main(){
    
    int *h_array , *h_mask, *h_result,  *s_result;
    int *d_array , *d_mask, *d_result ;
    
    float time;
    cudaEvent_t start, stop;
    cudaError_t cudaStat;

    int gridSize = (ARRAYSIZE  + BLOCKSIZE - 1) / BLOCKSIZE; 

    printf("\nVector size: %d, Mask size: %d, Block size: %d, Grid size: %d \n", ARRAYSIZE, MASKSIZE, BLOCKSIZE, gridSize);

    h_array = (int*) malloc ( ARRAYSIZE * ARRAYSIZE * sizeof(int) );
    h_mask  = (int*) malloc ( MASKSIZE  * MASKSIZE  * sizeof(int) );
    h_result= (int*) malloc ( ARRAYSIZE * ARRAYSIZE * sizeof(int) );
    s_result= (int*) malloc ( ARRAYSIZE * ARRAYSIZE * sizeof(int) );

    for (int i=0; i<ARRAYSIZE; i++)
       for (int j=0; j<ARRAYSIZE; j++)
            h_array[(i*ARRAYSIZE) + j ] =  rand()% 10; // ARRAYSIZE;

    for (int i=0; i<MASKSIZE; i++)
       for (int j=0; j<MASKSIZE; j++)
            h_mask[ (i*MASKSIZE) + j] =  rand()% 10; // ARRAYSIZE;


    printf("\nMatrix : \n");
    for (int i=0; i< ARRAYSIZE; i ++){
        for (int j=0; j< ARRAYSIZE; j ++)
              printf("%d ", h_array[(i*ARRAYSIZE) + j ]);
        printf("\n");
    }

    printf("\nMask : \n");
    for (int i=0; i< MASKSIZE; i ++){
        for (int j=0; j< MASKSIZE; j ++)
             printf("%d ", h_mask[(i*MASKSIZE) + j]);
        printf("\n");  
    }
      

    sequential_convolution (ARRAYSIZE, MASKSIZE , h_array , h_mask, s_result);

    printf("\nSequential result : \n");
    for (int i=0; i< ARRAYSIZE; i ++){
        for (int j=0; j< ARRAYSIZE; j ++)
            printf(" %4d ", s_result[(i*ARRAYSIZE) + j ]);
        printf("\n");   
    } 
   
    cudaStat = cudaMalloc ( (void **) & d_array,  ARRAYSIZE *  ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_mask,  MASKSIZE  *  MASKSIZE  *  sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       cudaFree(d_array);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_result,  ARRAYSIZE * ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       cudaFree(d_array);
       cudaFree(d_mask);
       return EXIT_FAILURE;
    }

    cudaMemcpy ( d_array , h_array,   ARRAYSIZE * ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy ( d_mask  , h_mask,    MASKSIZE  * MASKSIZE  * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy ( d_result, h_result,  ARRAYSIZE * ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
        
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    dim3  gridDims(gridSize, gridSize);
    dim3  blockDims (BLOCKSIZE, BLOCKSIZE);

    convolutionTwoDimension  <<<gridDims, blockDims >>>(ARRAYSIZE, MASKSIZE, d_array,  d_mask, d_result );
    cudaStat  = cudaGetLastError();
    if (cudaStat != cudaSuccess){
          printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
          free(h_array);
          free(h_mask);
          free(h_result);
          free(s_result);
          cudaFree(d_array);
          cudaFree(d_mask); 
          cudaFree(d_result);       
          return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel run Time: %.3f ms\n", time);
         
    cudaStat = cudaMemcpy(h_result, d_result, ARRAYSIZE * ARRAYSIZE * sizeof(int) , cudaMemcpyDeviceToHost);
    
    printf("\nParallel result : \n");
    for (int i=0; i< ARRAYSIZE; i ++){
        for (int j=0; j< ARRAYSIZE; j ++)
             printf(" %4d ", h_result[(i*ARRAYSIZE) + j]);
        printf("\n");    
    }

    printf("\nVerify result: ");
    if (comapreResults(ARRAYSIZE * ARRAYSIZE, h_result , s_result))
       printf("True\n");
    else
       printf("False\n");
 
    free(h_array);
    free(h_mask);
    free(h_result);
    free(s_result);
    cudaFree(d_array);
    cudaFree(d_mask);
    cudaFree(d_result);  
    return 0;
}
