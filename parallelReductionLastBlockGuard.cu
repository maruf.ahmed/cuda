/*****************************************************
Parallel reduction with the last block guard 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define ARRAYSIZE 100000 
#define BLOCKSIZE 128 
#define GRIDSIZE  64 

__device__ bool lastBlockGuard(int * counter){
    
    __threadfence();

   int totalLastCount = 0;
   if (threadIdx.x == 0)
        totalLastCount = atomicAdd(counter, 1);
   return __syncthreads_or(totalLastCount == gridDim.x-1);   
}

__global__ void sumMultiBlockWithLastGuard(long N, const long * a, long * b, int * lastBlockCounter){
    long localThreadIdx  = threadIdx.x;
    long globalThreadIdx = localThreadIdx + blockIdx.x * BLOCKSIZE; 
    long gridSize = BLOCKSIZE * gridDim.x;

    long gridSum = 0;
    for(long i = globalThreadIdx; i < N; i += gridSize)
       gridSum += a[i];
 
    __shared__ long sharedArray[BLOCKSIZE*sizeof(long)];
    sharedArray[localThreadIdx] = gridSum;
    __syncthreads();

    for (long offset = BLOCKSIZE/2; offset > 0; offset = offset/2){
        if (localThreadIdx < offset)
            sharedArray[localThreadIdx] += sharedArray[localThreadIdx + offset];
        __syncthreads();
    }    

    if(localThreadIdx == 0)
        b[blockIdx.x] = sharedArray[0];

    /**/ 
    if ( lastBlockGuard (lastBlockCounter)){
        if (localThreadIdx < GRIDSIZE){
            sharedArray[localThreadIdx] = b[localThreadIdx];
        }
        else{
            sharedArray[localThreadIdx] = 0;
        }
        __syncthreads();

        for(long offset = BLOCKSIZE / 2; offset > 0; offset /=2) {
            if (localThreadIdx < offset )
                sharedArray[localThreadIdx] += sharedArray[localThreadIdx+offset];
            __syncthreads();    
        }
        if (localThreadIdx == 0)
            b[0] = sharedArray[0];
    }
    /**/  
}


void kernelErrorPrint(){
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess){
        printf("Kernel execution Error: %s\n", cudaGetErrorString(err));
        exit(-1);
    }
}

void cudaErrorPrint(cudaError_t cudaERR){
    if (cudaERR != cudaSuccess)
      printf ("Cuda Error : %s \n", cudaGetErrorString(cudaERR));
}

int main(){
    
    long * h_a, * h_b, sum;
    long * d_a, * d_b;
    int  * d_lastBlockCounter;

    h_a = (long*) malloc ( ARRAYSIZE * sizeof(long));
    h_b = (long*) malloc ( GRIDSIZE * sizeof(long));
    
    printf("\nArray: \n");
    for(int i=0; i< ARRAYSIZE; i ++){
        h_a[i] = rand() % ARRAYSIZE;
        
        printf("%ld ", h_a[i]);
    }
    printf("\n");
    cudaErrorPrint( cudaMalloc( (void **) &d_a, ARRAYSIZE* sizeof (long)) ); 
    cudaErrorPrint( cudaMalloc( (void **) &d_b, GRIDSIZE * sizeof (long)) );
    cudaErrorPrint( cudaMalloc( (void **) &d_lastBlockCounter, sizeof(int)) );

    cudaMemcpy( d_a , h_a , ARRAYSIZE * sizeof(long) , cudaMemcpyHostToDevice  );
    cudaMemset( d_lastBlockCounter, 0, sizeof(int));

    sumMultiBlockWithLastGuard<<<GRIDSIZE, BLOCKSIZE>>>(ARRAYSIZE, d_a, d_b , d_lastBlockCounter);
    kernelErrorPrint();
    cudaDeviceSynchronize();

    /*********************************************************************
    cudaMemcpy(h_b, d_b, GRIDSIZE * sizeof(long), cudaMemcpyDeviceToHost);
    printf("\nArray B: \n");
    for(int i=0; i< GRIDSIZE; i ++)
        printf("%ld ", h_b[i]);
    *********************************************************************/

    cudaMemcpy(&sum, d_b, sizeof(long), cudaMemcpyDeviceToHost);

    printf("\nSum: %ld\n", sum);

    free(h_a);
    free(h_b);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_lastBlockCounter); 

    return 0;
}
