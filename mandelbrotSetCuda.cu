/********************************************
Mandelbrot Set in Cuda 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <chrono> 
#include <iostream> 

#define BLOCKSIZE       (1024L)
#define BAILOUT_RADIUS  (4)
#define MAX_ITER        (50000)
    


static void HandleError( cudaError_t cudaStat, const char *file, int line ) {
  if (cudaStat != cudaSuccess) {
     printf ("Cuda Error : %s in File: %s, at Line: %d\n", cudaGetErrorString(cudaStat), file, line );
     exit( EXIT_FAILURE );
  }
}
#define HANDLE_ERROR( cudaStat ) (HandleError( cudaStat, __FILE__, __LINE__ ))


typedef struct {
    int width;
    int height;
    float x_max;
    float x_min;
    float y_max;
    float y_min;
    float c_re;
    float c_im;
} Dimensions;

__constant__ Dimensions dev_d;

__global__ void mandelbrot(float *pixels) {
    
    int globalIdx, xi , yi;
    float Cx , Cy;

    float x , y , tmp, i;
   
    globalIdx = threadIdx.x + blockIdx.x * blockDim.x; 
    
    xi = globalIdx % dev_d.height;
    yi = globalIdx / dev_d.width;
  
    Cx = (float)xi*(dev_d.x_max - dev_d.x_min)/((float)dev_d.width)  + (float)dev_d.x_min;
    Cy = (float)yi*(dev_d.y_max - dev_d.y_min)/((float)dev_d.height) + (float)dev_d.y_min ;
   
    x = y = i = 0;
      
    while(x*x + y*y < BAILOUT_RADIUS && i < MAX_ITER) {
        
        tmp = x*x - y*y + Cx;
        y = 2*x*y + Cy;
        x = tmp;
        ++i;
    }
    
   if(i == MAX_ITER) 
        pixels[globalIdx] = (float) i;
       
   else 
        pixels[globalIdx] = (float) i + 1.5 - log2(log2(x*x+y*y)/2);
        
}
	

int main(){
      
    float *d_pixels, *h_pixels; 
  
    float time;
    cudaEvent_t start, stop;

    Dimensions h_dim = { .width=800, .height=800, 
                         .x_max=1.5, .x_min=-2.5,
                         .y_max=2.0, .y_min=-2.0,
                         .c_re=0.2,  .c_im=0.2
                       }; 

    long size  = h_dim.width * h_dim.height;
 
    long GRIDSIZE = (size + BLOCKSIZE -1)/BLOCKSIZE;

    printf("\nsize: %ld, Block size: %ld, grid size: %ld \n", size, BLOCKSIZE, GRIDSIZE);

    h_pixels = (float*) malloc(size * sizeof(float));  
    HANDLE_ERROR( cudaMalloc((void **) &d_pixels, size * sizeof(float)) );  

    cudaMemcpyToSymbol(dev_d, &h_dim , sizeof(Dimensions), 0, cudaMemcpyHostToDevice);
    //HANDLE_ERROR( cudaGetLastError() );
 
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);
    
    mandelbrot <<< GRIDSIZE, BLOCKSIZE >>> (d_pixels);
    HANDLE_ERROR( cudaGetLastError() );
    cudaDeviceSynchronize();
  
    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    cudaMemcpy(h_pixels, d_pixels, size * sizeof(float), cudaMemcpyDeviceToHost);

    FILE * fp;
    const char *filename="mandelbrot.ppm";
    const char *comment="# ";
    const int MaxColorComponentValue =255; 
    fp= fopen(filename,"wb"); 
    fprintf(fp,"P6\n %s\n %d\n %d\n %d\n", comment, h_dim.width, h_dim.height, MaxColorComponentValue);

    
    float pixel;
    int   color[3]= {0,0,0}; 
    float maxP    = 5.15;    // MAX_ITER ;
    //float maxP1 = 2.5, maxP2    = 4.1; 
    for(int i=0;i<size;i++){
         
          pixel = h_pixels[i];
         
          /*****************************************  
          color[2] = col < 255 ?  col: 255 ;     
          color[1] = col < 510 ?  col-255: 255 ; 
          color[0] = col - 510; 
          ******************************************/
          /*****************************************/  
          color[2] = pixel < maxP/3     ?  (pixel/maxP)*255 : 255 ;     
          color[1] = pixel < maxP * 2/3 ?  ((pixel  -maxP)/maxP)*255 : 255 ; 
          color[0] = ((pixel - maxP * 2/3)/maxP)*255; 
          /*****************************************/  
          /*****************************************  
          color[2] = pixel < maxP1 ?  (pixel/maxP1)*255 : 255 ;     
          color[1] = pixel < maxP2 ?  ((pixel  - maxP1)/(maxP2 -maxP1))*255 : 255 ; 
          color[0] = ((pixel - maxP2  )/(MAX_ITER- maxP2))*255; 
          *****************************************/  


          fwrite(&color,sizeof(char),3,fp);
     }

 
    free(h_pixels);
    cudaFree(d_pixels);
    return 0;
}
