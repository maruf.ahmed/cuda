/********************************************
Sum Reduction in Cuda with a device function to optimize the warp performance.
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>

#define BLOCKSIZE  (32)
#define ARRAYSIZE  (256*256)

__device__ void warpReduce (volatile long * sharedData, long localThreadIdx){
    
    sharedData [localThreadIdx] += sharedData [localThreadIdx + 32];
    sharedData [localThreadIdx] += sharedData [localThreadIdx + 16];
    sharedData [localThreadIdx] += sharedData [localThreadIdx + 8];
    sharedData [localThreadIdx] += sharedData [localThreadIdx + 4];
    sharedData [localThreadIdx] += sharedData [localThreadIdx + 2];
    sharedData [localThreadIdx] += sharedData [localThreadIdx + 1];
}

__global__ void sumReductionWithWarpReduce (long * a, long * sum_a){
     
    long localThreadIdx = threadIdx.x;
   
    __shared__ long sharedArray[BLOCKSIZE * sizeof(long)]; 
        
    long localStride = ( blockIdx.x * (2 * blockDim.x)  )+  localThreadIdx;
  
    sharedArray[localThreadIdx] = a[localStride] + a[localStride + blockDim.x]; 
    a[localStride] =  0; 
    a[localStride + blockDim.x] =  0; 

    __syncthreads ();
    
    for(long offset = blockDim.x/2; offset > 32; offset /= 2 ){
       
        if (localThreadIdx < offset)
              sharedArray[localThreadIdx] += sharedArray[localThreadIdx + offset];
        __syncthreads() ;   
    }

    if (localThreadIdx < 32)
       warpReduce(sharedArray, localThreadIdx);
      
    if (localThreadIdx == 0)
       sum_a [blockIdx.x]  = sharedArray [ 0 ];
}


int main(){
    
    long * h_a , *h_b ;
    long * d_a , * d_b;
    cudaError_t cudaStat;
    long gridSize = (ARRAYSIZE / 2 + BLOCKSIZE - 1) / BLOCKSIZE; 
    float time;
    cudaEvent_t start, stop;

    printf("\nVector size: %d, Block size: %d, Grid size: %ld \n", ARRAYSIZE, BLOCKSIZE, gridSize);

    h_a = (long*) malloc ( ARRAYSIZE * sizeof(long));
    h_b = (long*) malloc ( ARRAYSIZE * sizeof(long));

    for (long i=0; i<ARRAYSIZE; i++)
       h_a[i] = rand()% 1000; // ARRAYSIZE;

    printf("\nVector A: \n");
    for (long i=0; i< ARRAYSIZE; i ++)
        printf("%ld ", h_a[i]);
    printf("\n");    
   
    cudaStat = cudaMalloc ( (void **) & d_a,  ARRAYSIZE * sizeof(long) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_b, ARRAYSIZE  * sizeof (long) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       return EXIT_FAILURE;
    }

    //cudaMemcpy ( d_a , h_a, ARRAYSIZE * sizeof(long), cudaMemcpyHostToDevice);
    cudaMemcpy ( d_b , h_a, ARRAYSIZE * sizeof(long), cudaMemcpyHostToDevice);
    
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    do {
        sumReductionWithWarpReduce <<<gridSize, BLOCKSIZE >>>(d_b, d_b );
        cudaStat  = cudaGetLastError();
        if (cudaStat != cudaSuccess){
            printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
            free(h_a);
            free(h_b);
            cudaFree(d_a);
            cudaFree(d_b);        
            return EXIT_FAILURE;
         }
         cudaDeviceSynchronize();

        /******************************************  
         cudaStat = cudaMemcpy(h_b, d_b, ARRAYSIZE * sizeof(long), cudaMemcpyDeviceToHost);
         printf("%ld\n", gridSize); 
         for (long i=0; i< ARRAYSIZE; i ++)
              printf("%ld ", h_b[i]);
         printf("\n");  
         ******************************************/  
         gridSize = (gridSize + BLOCKSIZE -1) /BLOCKSIZE;
         
    } while ( gridSize - 1 > 0 );
     
    sumReductionWithWarpReduce <<<1, BLOCKSIZE >>>(d_b, d_b);
    cudaStat =  cudaGetLastError();
    if (cudaStat != cudaSuccess){
        printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
        free(h_a);
        free(h_b);
        cudaFree(d_a);
        cudaFree(d_b);
        return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nTime: %.3f ms\n", time);
     
    cudaStat = cudaMemcpy(h_b, d_b, ARRAYSIZE  * sizeof(long), cudaMemcpyDeviceToHost);
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       cudaFree(d_a);
       cudaFree(d_b);
       return EXIT_FAILURE;
    }

    printf("\nSum: %ld\n", h_b[0]); 

    free(h_a);
    free(h_b);
    cudaFree(d_a);
    cudaFree(d_b);
    return 0;
}
