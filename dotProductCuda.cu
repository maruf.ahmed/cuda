/********************************************
Finding dot porduct in Cuda 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <chrono> 
#include <iostream> 

#define BLOCKSIZE  (32L)
#define GRIDSIZE   (32L)
#define ARRAYSIZE   (1024L*1024L*64L)

static void HandleError( cudaError_t cudaStat, const char *file, int line ) {
  if (cudaStat != cudaSuccess) {
     printf ("Cuda Error : %s in File: %s, at Line: %d\n", cudaGetErrorString(cudaStat), file, line );
     exit( EXIT_FAILURE );
  }
}
#define HANDLE_ERROR( cudaStat ) (HandleError( cudaStat, __FILE__, __LINE__ ))


__global__ void dotProduct (long N, double * a, double * b, double * sum){
     
    long  globalThreadIdx =  blockIdx.x * blockDim.x +  threadIdx.x;
    long  offset; 
    double   localSum;

    __shared__ double sharedArray[BLOCKSIZE]; 

    offset = 0;
    localSum = 0.0;
    
    while (globalThreadIdx + offset < N){
        localSum += a [globalThreadIdx + offset] * b [globalThreadIdx + offset] ;
        offset +=  gridDim.x * blockDim.x;
    }
   
   sharedArray[threadIdx.x] = localSum ; 
 
    __syncthreads ();
       
    offset =  blockDim.x / 2;  
    while (offset > 0 ){
        if (threadIdx.x < offset )
              sharedArray[threadIdx.x ] += sharedArray[threadIdx.x + offset] ;
        __syncthreads() ;  

        offset = offset/2;
    }
    
    if (threadIdx.x == 0) 
       atomicAdd(sum, sharedArray[ 0 ]);
     
}

void SeqDotProduct (long N, double * a, double * b, double * sum){
      double   localSum = 0.0;
      for(unsigned i = 0; i< N; i++)
            localSum += a[i] * b[i];
      *sum = localSum; 
}


int main(){
    
    double * h_a , * h_b, * h_c , * seq_c ;
    double * d_a , * d_b, * d_c ; 
  
    float time;
    cudaEvent_t start, stop;

    printf("\nVector size: %ld, Block size: %ld, grid size: %ld \n", ARRAYSIZE, BLOCKSIZE, GRIDSIZE);

    h_a = (double*) malloc ( ARRAYSIZE * sizeof(double));
    h_b = (double*) malloc ( ARRAYSIZE * sizeof(double));
    h_c = (double*) malloc ( sizeof(double));
    seq_c = (double*) malloc ( sizeof(double));

    for (long i=0; i<ARRAYSIZE; i++){
       h_a[i] = (double) ( rand() % ARRAYSIZE ) / (ARRAYSIZE);
       h_b[i] = (double) ( rand() % ARRAYSIZE ) / (ARRAYSIZE);
    }

    printf("\nVector A: \n");
    for (long i=0; i< ARRAYSIZE; i ++)
        printf("%.3lf ", h_a[i]);
    printf("\n");  

    printf("\nVector B: \n");
    for (long i=0; i< ARRAYSIZE; i ++)
        printf("%.3lf ", h_b[i]);
    printf("\n"); 

   {
       using namespace std::chrono; 

       auto start_seq = high_resolution_clock::now(); 

       SeqDotProduct (ARRAYSIZE, h_a, h_b,  seq_c);
    
       auto stop_seq = high_resolution_clock::now(); 

       auto duration_seq = duration_cast<microseconds>(stop_seq - start_seq); 
       auto int_ms =       duration_cast<milliseconds>(stop_seq - start_seq);

       std::cout <<"Time:" << duration_seq.count() << " microseconds, or" << std::endl; 
       std::cout <<"    :"  << int_ms.count() << " milliseconds, or" << std::endl; 
       std::cout <<"    :"  << (double) duration_seq.count() /1000000 << " seconds" << std::endl; 
    }    

    printf("\nSeq sum: %lf\n", *seq_c); 
   
    HANDLE_ERROR( cudaMalloc ( (void **) & d_a,  ARRAYSIZE * sizeof(double) ) );
    HANDLE_ERROR( cudaMalloc ( (void **) & d_b,  ARRAYSIZE * sizeof(double) ) );
    HANDLE_ERROR( cudaMalloc ( (void **) & d_c,  sizeof(double) ) );
   

    cudaMemcpy ( d_a , h_a,  ARRAYSIZE * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy ( d_b , h_b,  ARRAYSIZE * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemset ( d_c , 0, sizeof(double));
    
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    dotProduct  <<<GRIDSIZE, BLOCKSIZE >>>(ARRAYSIZE, d_a, d_b,  d_c) ;
    HANDLE_ERROR( cudaGetLastError() );
    cudaDeviceSynchronize();
  
    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel Time: %.3f ms\n", time);
     
    cudaMemcpy( h_c, d_c, sizeof(double), cudaMemcpyDeviceToHost) ;
    HANDLE_ERROR( cudaGetLastError() );

    printf("\nSum: %lf\n", *h_c); 

    free(h_a);
    free(h_b);
    free(h_c);
    free(seq_c);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    return 0;
}
