/********************************************
Monte Carlo Simulation in Cuda 

Must compile with the options: -lcurand -arch=sm_61

Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
#include <random>
#include <inttypes.h>
#include <time.h>
#include <chrono> 
#include <iostream> 

#define BLOCKSIZE  (32L)
#define GRIDSIZE   (1024L)
#define SIM_SIZE   (1024L*1024L*1024L *256L)



__global__ void setCurandRandStates  (curandState *state){
     
    unsigned  globalThreadIdx =  blockIdx.x * blockDim.x +  threadIdx.x;

    curand_init ( clock64()  , globalThreadIdx, 0, &state[globalThreadIdx]);
}


__global__ void MonteCarloPiCuda (long long M, double  *circle_points, curandState *state){
     
    unsigned globalThreadIdx =  blockIdx.x * blockDim.x +  threadIdx.x;
    unsigned offset;

    __shared__  double sharedArray[BLOCKSIZE]; 
    sharedArray[threadIdx.x] = 0;
    __syncthreads ();
    
    double x, y, r;
    for (long long i = 0; i < M ; i++){
         x = curand_uniform (&state [globalThreadIdx]);
         y = curand_uniform (&state [globalThreadIdx]);
         r = x*x + y*y;

         if (r<=1.0)
           sharedArray[threadIdx.x] ++;
    }
    __syncthreads ();
   
    offset =  blockDim.x / 2;  
    while (offset > 0 ){
        if (threadIdx.x < offset )
              sharedArray[threadIdx.x ]  += sharedArray[threadIdx.x + offset] ;
            
        __syncthreads() ;  

        offset = offset/2;
    }
       
    if (threadIdx.x == 0)
        atomicAdd ( circle_points, sharedArray[0]);
      
}

 void sequentialMonteCarloPi (long long N, long double *pi ){
    
    long double x, y, r; 
    long long   square_points, circle_points;
    long progress = N/100;
  
    {
       using namespace std;
       default_random_engine rand_eng;
       uniform_real_distribution <long double> gen_rand(0,1.0);
       
       circle_points = 0;
       printf("\n");
       for (square_points = 0; square_points < N; square_points++){
           x = gen_rand (rand_eng);
           y = gen_rand (rand_eng);
           r = x*x + y*y;
            
           if ( r<= 1.0)
              circle_points ++; 

           if (square_points % progress == 0 )
              printf("=");   
       }
       printf("\n");
       *pi = 4 *  (long double) circle_points / square_points;
    };


}



int main(){
    
    long double  *seq_pi ;
    double  * h_pi;
    long double  * d_pi ; 
    
    double   *h_circle_points , *d_circle_points ; 
    curandState  *d_state;
    
    //cudaError_t cudaStat;
    float time;
    cudaEvent_t start, stop;
 
    printf("Simulation points: %ld, Block size: %ld, grid size: %ld \n", (long) SIM_SIZE, (long) BLOCKSIZE, (long) GRIDSIZE);
  
    h_pi   = (double*) malloc (  sizeof(double ));
    seq_pi = (long double*) malloc (  sizeof(long double ));
  
    h_circle_points = (double *) malloc (  sizeof( double ) );

    {
       using namespace std::chrono; 

       auto start_seq = high_resolution_clock::now(); 

       sequentialMonteCarloPi (SIM_SIZE, seq_pi)  ;

       auto stop_seq = high_resolution_clock::now(); 

       auto duration_seq = duration_cast<microseconds>(stop_seq - start_seq); 

       std::cout <<"Time:" << duration_seq.count() << " microseconds, or" << std::endl; 
       std::cout <<"    :" << (double) duration_seq.count() /1000000 << " seconds" << std::endl; 
    }

    printf("Pi (Sequential): %Lf \n", *seq_pi);
   
    if ( cudaMalloc ( (void **) & d_pi,  sizeof(long double) )  != cudaSuccess ) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
       free(h_pi);
       return EXIT_FAILURE;
    }


    if ( cudaMalloc ( (void **) & d_circle_points,  sizeof(double) )!= cudaSuccess ) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
       free(h_pi);
       return EXIT_FAILURE;
    }

    if (cudaMalloc ( (void **) & d_state, GRIDSIZE * BLOCKSIZE * sizeof (curandState) )!= cudaSuccess ) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
       free(h_pi);
       cudaFree(d_pi);
       return EXIT_FAILURE;
    }

    if ( cudaMemset (  d_pi , 0, sizeof(long double) )!= cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
       free(h_pi);
       cudaFree(d_pi);
       return EXIT_FAILURE;
    }   
    if ( cudaMemset ( d_circle_points , 0, sizeof(double) )!= cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
       free(h_pi);
       cudaFree(d_pi);
       return EXIT_FAILURE;
    }
      
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    setCurandRandStates <<<GRIDSIZE, BLOCKSIZE >>>(d_state);
    if ( cudaGetLastError()!= cudaSuccess){
          printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
          free(h_pi);
          cudaFree(d_pi);     
          return EXIT_FAILURE;
     }
    cudaDeviceSynchronize();

    long long M = SIM_SIZE / (GRIDSIZE * BLOCKSIZE);

    MonteCarloPiCuda <<<GRIDSIZE, BLOCKSIZE >>>(M, d_circle_points, d_state);
    if ( cudaGetLastError() != cudaSuccess){
          printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
          free(h_pi);
          cudaFree(d_pi);     
          return EXIT_FAILURE;
     }
    cudaDeviceSynchronize();
  
    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel Time: %.3f ms\n", time);
     
    if ( cudaMemcpy(h_circle_points, d_circle_points, sizeof(double ), cudaMemcpyDeviceToHost)!= cudaSuccess){
         printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaPeekAtLastError()), __FILE__,  __LINE__ -1);
         free(h_pi);
         cudaFree(d_pi);           
         return EXIT_FAILURE;
     }
 
   
    *h_pi = 4 *  ( *h_circle_points)  /  SIM_SIZE;
    printf("\npi (parallel): %f\n", *h_pi); 

    
    free(h_pi);
    cudaFree(d_pi);     
    return 0;
}
