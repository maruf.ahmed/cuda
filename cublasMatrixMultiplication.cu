/********************************************
cublas Matrix operations 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <device_launch_parameters.h>
#include <curand.h>

#define MATRIXSIZE (4*4)


static const char *_cudaGetErrorEnum(cublasStatus_t error){
    switch (error){
        
        case CUBLAS_STATUS_SUCCESS:
            return "CUBLAS_STATUS_SUCCESS";

        case CUBLAS_STATUS_NOT_INITIALIZED:
            return "CUBLAS_STATUS_NOT_INITIALIZED";

        case CUBLAS_STATUS_ALLOC_FAILED:
            return "CUBLAS_STATUS_ALLOC_FAILED";

        case CUBLAS_STATUS_INVALID_VALUE:
            return "CUBLAS_STATUS_INVALID_VALUE";

        case CUBLAS_STATUS_ARCH_MISMATCH:
            return "CUBLAS_STATUS_ARCH_MISMATCH";

        case CUBLAS_STATUS_MAPPING_ERROR:
            return "CUBLAS_STATUS_MAPPING_ERROR";

        case CUBLAS_STATUS_EXECUTION_FAILED:
            return "CUBLAS_STATUS_EXECUTION_FAILED";

        case CUBLAS_STATUS_INTERNAL_ERROR:
            return "CUBLAS_STATUS_INTERNAL_ERROR";
    }

    return "<unknown>";
}

void printCublasMatrix(int N, float *a){
    for (int i=0; i < N; i ++){
      printf("\n");
      for (int j= 0; j < N; j++)
          printf ( "%10.4f", a [N*j + i] );
    }
    printf("\n");
}

 
int main(){
    
    float * h_a, * h_b, * h_c;
    float * d_a, * d_b, * d_c;
    cudaError_t cudaStat;
    cublasStatus_t cublasStat;
    cublasHandle_t handle;
    float alpha, beta;
   
    /** Allocate Memory for the host  **/
    h_a = ( float *) malloc (MATRIXSIZE * MATRIXSIZE *sizeof (float));
    h_b = ( float *) malloc (MATRIXSIZE  * MATRIXSIZE *sizeof (float));
    h_c = ( float *) malloc (MATRIXSIZE * MATRIXSIZE *sizeof (float));

    /** Allocate Memory for the device  **/
    cudaStat = cudaMalloc((void **) & d_a , (MATRIXSIZE * MATRIXSIZE *sizeof (float)) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       free(h_c);
       return EXIT_FAILURE;
    }
    cudaStat = cudaMalloc((void **) & d_b , (MATRIXSIZE * MATRIXSIZE *sizeof (float)) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       return EXIT_FAILURE;
    }
    cudaStat = cudaMalloc((void **) & d_c , (MATRIXSIZE * MATRIXSIZE *sizeof (float)) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       return EXIT_FAILURE;
    }   
 
   curandGenerator_t curandHandle;
   curandCreateGenerator(&curandHandle, CURAND_RNG_PSEUDO_DEFAULT);

   curandSetPseudoRandomGeneratorSeed(curandHandle, (unsigned long long) clock());

   curandGenerateUniform(curandHandle, d_a, MATRIXSIZE * MATRIXSIZE );  
   curandGenerateUniform(curandHandle, d_b, MATRIXSIZE * MATRIXSIZE );  
   curandGenerateUniform(curandHandle, d_c, MATRIXSIZE * MATRIXSIZE );  

   cublasStat = cublasGetVector( MATRIXSIZE * MATRIXSIZE , sizeof(float),  d_a, 1,  h_a, 1);
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cudaFree(d_c);
       return EXIT_FAILURE;
   }

   cublasStat = cublasGetVector( MATRIXSIZE * MATRIXSIZE , sizeof(float),  d_b, 1,  h_b, 1);
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cudaFree(d_c);
       return EXIT_FAILURE;
   }

   cublasStat = cublasGetVector( MATRIXSIZE * MATRIXSIZE , sizeof(float),  d_c, 1,  h_c, 1);
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cudaFree(d_c);
       return EXIT_FAILURE;
   }
 
   printf("Matrix A, ");
   printf("Dimension: %d x %d\n", MATRIXSIZE, MATRIXSIZE);
   /***********************************************
   for(int i = 0; i < MATRIXSIZE * MATRIXSIZE; i++) 
        printf("%10.4f", h_a[i]);
   printf("\n");
   ***********************************************/  
   printCublasMatrix(MATRIXSIZE, h_a);
   printf("\n");

   printf("Matrix B, ");
   printf("Dimension: %d x %d\n", MATRIXSIZE, MATRIXSIZE);
   /***********************************************
   for(int i = 0; i < MATRIXSIZE * MATRIXSIZE; i++) 
        printf("%10.4f", h_b[i]);
   printf("\n");
   ***********************************************/  
   printCublasMatrix(MATRIXSIZE, h_b);
   printf("\n");

   printf("Matrix C, ");
   printf("Dimension: %d x %d\n", MATRIXSIZE, MATRIXSIZE);
   /***********************************************
   for(int i = 0; i < MATRIXSIZE * MATRIXSIZE; i++) 
        printf("%10.4f", h_c[i]);
   printf("\n");
   ***********************************************/  
   printCublasMatrix(MATRIXSIZE, h_c);
   printf("\n");


   cublasStat = cublasCreate_v2(&handle);
   if (cublasStat != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n",_cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cudaFree(d_c);
       return EXIT_FAILURE;
   }
  
   alpha = 3.0;
   beta  = 2.0;

   /************************************************/
   cublasStat = cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, MATRIXSIZE, MATRIXSIZE, MATRIXSIZE, &alpha, d_a, MATRIXSIZE, d_b, MATRIXSIZE, &beta, d_c, MATRIXSIZE );
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cudaFree(d_c);
       cublasDestroy(handle);
       return EXIT_FAILURE;
   }
  
   cublasStat = cublasGetVector( MATRIXSIZE * MATRIXSIZE , sizeof(float),  d_c, 1,  h_c, 1);
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cudaFree(d_c);
       cublasDestroy(handle);
       return EXIT_FAILURE;
   }
   
   printf("Matrix ( (A * %.2f)*B + C* %.2f):\n", alpha, beta);
   /*********************************************** 
   for(int i = 0; i < MATRIXSIZE * MATRIXSIZE; i++) 
        printf("%10.4f", h_c[i]);
   printf("\n");
   ***********************************************/  
   printCublasMatrix(MATRIXSIZE, h_c);

   free(h_a);
   free(h_b);
   free(h_c);
   cudaFree(d_a);
   cudaFree(d_b);
   cudaFree(d_c);
   return 0;
}
