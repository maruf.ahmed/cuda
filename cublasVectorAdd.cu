/********************************************
cublas Vector Addition 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <device_launch_parameters.h>

#define VECTORSIZE (64*64)


static const char *_cudaGetErrorEnum(cublasStatus_t error){
    switch (error){
        
        case CUBLAS_STATUS_SUCCESS:
            return "CUBLAS_STATUS_SUCCESS";

        case CUBLAS_STATUS_NOT_INITIALIZED:
            return "CUBLAS_STATUS_NOT_INITIALIZED";

        case CUBLAS_STATUS_ALLOC_FAILED:
            return "CUBLAS_STATUS_ALLOC_FAILED";

        case CUBLAS_STATUS_INVALID_VALUE:
            return "CUBLAS_STATUS_INVALID_VALUE";

        case CUBLAS_STATUS_ARCH_MISMATCH:
            return "CUBLAS_STATUS_ARCH_MISMATCH";

        case CUBLAS_STATUS_MAPPING_ERROR:
            return "CUBLAS_STATUS_MAPPING_ERROR";

        case CUBLAS_STATUS_EXECUTION_FAILED:
            return "CUBLAS_STATUS_EXECUTION_FAILED";

        case CUBLAS_STATUS_INTERNAL_ERROR:
            return "CUBLAS_STATUS_INTERNAL_ERROR";
    }

    return "<unknown>";
}
 
int main(){
    
    float * h_a, * h_b, * h_c;
    float * d_a, * d_b ;
    cudaError_t cudaStat;
    cublasStatus_t cublasStat;
    cublasHandle_t handle;
   
    /** Allocate Memory for the host  **/
    h_a = ( float *) malloc (VECTORSIZE * sizeof (float));
    h_b = ( float *) malloc (VECTORSIZE * sizeof (float));
    h_c = ( float *) malloc (VECTORSIZE * sizeof (float));

    /** Allocate Memory for the device  **/
    cudaStat = cudaMalloc((void **) & d_a , (VECTORSIZE * sizeof (float)) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       free(h_c);
       return EXIT_FAILURE;
    }
    cudaStat = cudaMalloc((void **) & d_b , (VECTORSIZE * sizeof (float)) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       return EXIT_FAILURE;
    }

 
    for(int i=0; i < VECTORSIZE; i++ ){
        h_a [i] = rand() % VECTORSIZE;
        h_b [i] = rand() % VECTORSIZE;  
    } 
   
   printf("Vector X, ");
   printf("Dimension: %d \n", VECTORSIZE);
   /***********************************************/
   for(int i = 0; i < VECTORSIZE; i++) 
        printf("%10.2f", h_a[i]);
   printf("\n");
   /***********************************************/

   printf("Matrix Y, ");
   printf("Dimension: %d \n", VECTORSIZE);
   /***********************************************/
   for(int i = 0; i < VECTORSIZE; i++) 
        printf("%10.2f", h_b[i]);
   printf("\n");
   /***********************************************/  
  
   cublasStat = cublasCreate_v2(&handle);
   if (cublasStat != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n",_cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       return EXIT_FAILURE;
   }
  
  cublasStat = cublasSetVector( VECTORSIZE, sizeof(float), h_a, 1, d_a, 1);
  if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cublasDestroy(handle);
       return EXIT_FAILURE;
   }

   cublasStat = cublasSetVector( VECTORSIZE, sizeof(float), h_b, 1, d_b, 1);
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cublasDestroy(handle);
       return EXIT_FAILURE;
   }

   float alpha = 3.0;
   cublasStat = cublasSaxpy( handle, VECTORSIZE, &alpha, d_a, 1, d_b, 1 );
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cublasDestroy(handle);
       return EXIT_FAILURE;
   }

   cublasStat = cublasGetVector( VECTORSIZE, sizeof(float),  d_b, 1,  h_c, 1);
   if (cublasStat  != CUBLAS_STATUS_SUCCESS) {
       printf ("Cublas Error: %s at File: %s, line %d\n", _cudaGetErrorEnum(cublasStat),  __FILE__,  __LINE__ );
       free(h_a);
       free(h_b);
       free(h_c);
       cudaFree(d_a);
       cudaFree(d_b);
       cublasDestroy(handle);
       return EXIT_FAILURE;
   }

   printf("Vector ( X * %.2f + Y):\n", alpha);
   /***********************************************/
   for(int i = 0; i < VECTORSIZE; i++) 
        printf("%10.2f", h_c[i]);
   printf("\n");
   /***********************************************/  
   free(h_a);
   free(h_b);
   free(h_c);
   cudaFree(d_a);
   cudaFree(d_b);
   return 0;
}
