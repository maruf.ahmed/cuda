/********************************************
One dimensional vector colvolution calculation in Cuda
 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <cooperative_groups.h>
#include <stdbool.h>

using namespace cooperative_groups;

#define BLOCKSIZE  (32)
#define ARRAYSIZE  (256*256)
#define MASKSIZE   (64)



__global__ void convolutionOneDimension (int N, int M, int * a, int * b, int * c){
  
    int globalIdx = blockIdx.x * blockDim.x + threadIdx.x;
    int convoSum = 0; 
    for (int j = 0; j < M ; j++)
        if (globalIdx - M/2 + j >= 0 && globalIdx - M/2 + j < N )
            convoSum += a [globalIdx - M/2 + j ] * b [j]; 
    c[globalIdx] = convoSum; 
}

void sequential_convolution (int N, int M, int * a, int * b, int * c){
    int convoSum; 
   
    for (int i=0; i<N; i++){
        convoSum = 0;
        for (int j = 0; j < M ; j++)
            if (i - M/2 + j >= 0 && i - M/2 + j < N )
                 convoSum += a [i - M/2 + j ] * b [j]; 
        c[i] = convoSum;
    }
}

bool comapreResults(int N, int *a , int *b){
    
    for (int i=0; i<N; i++)
      if (a[i] != b [i])
         return false;
   return true;       
}

int main(){
    
    int *h_array , *h_mask, *h_result,  *s_result;
    int *d_array , *d_mask, *d_result ;
    
    float time;
    cudaEvent_t start, stop;
    cudaError_t cudaStat;

    int gridSize = (ARRAYSIZE  + BLOCKSIZE - 1) / BLOCKSIZE; 

    printf("\nVector size: %d, Mask size: %d, Block size: %d, Grid size: %d \n", ARRAYSIZE, MASKSIZE, BLOCKSIZE, gridSize);

    h_array = (int*) malloc ( ARRAYSIZE * sizeof(int) );
    h_mask  = (int*) malloc ( MASKSIZE  * sizeof(int) );
    h_result= (int*) malloc ( ARRAYSIZE * sizeof(int) );
    s_result= (int*) malloc ( ARRAYSIZE * sizeof(int) );

    for (int i=0; i<ARRAYSIZE; i++)
       h_array[i] =  rand()% 10; // ARRAYSIZE;

    for (int i=0; i<MASKSIZE; i++)
       h_mask[i] =  rand()% 10; // ARRAYSIZE;


    printf("\nVector : \n");
    for (int i=0; i< ARRAYSIZE; i ++)
        printf("%d ", h_array[i]);
    printf("\n");

    printf("\nMask : \n");
    for (int i=0; i< MASKSIZE; i ++)
        printf("%d ", h_mask[i]);
    printf("\n");    

   sequential_convolution (ARRAYSIZE, MASKSIZE , h_array , h_mask, s_result);

    printf("\nSequential result : \n");
    for (int i=0; i< ARRAYSIZE; i ++)
        printf("%d ", s_result[i]);
    printf("\n");    
   
    cudaStat = cudaMalloc ( (void **) & d_array,  ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_mask,  MASKSIZE  *  sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       cudaFree(d_array);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_result,  ARRAYSIZE * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       cudaFree(d_array);
       cudaFree(d_mask);
       return EXIT_FAILURE;
    }

    cudaMemcpy ( d_array , h_array,   ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy ( d_mask  , h_mask,    MASKSIZE  * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy ( d_result, h_result,  ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
        
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    convolutionOneDimension  <<<gridSize, BLOCKSIZE >>>(ARRAYSIZE, MASKSIZE, d_array,  d_mask, d_result );
    cudaStat  = cudaGetLastError();
    if (cudaStat != cudaSuccess){
          printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
          free(h_array);
          free(h_mask);
          free(h_result);
          free(s_result);
          cudaFree(d_array);
          cudaFree(d_mask); 
          cudaFree(d_result);       
          return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel run Time: %.3f ms\n", time);
         
    cudaStat = cudaMemcpy(h_result, d_result, ARRAYSIZE * sizeof(int) , cudaMemcpyDeviceToHost);
    
    printf("\nParallel result : \n");
    for (int i=0; i< ARRAYSIZE; i ++)
        printf("%d ", h_result[i]);
    printf("\n");    

    printf("\nVerify result: ");
    if (comapreResults(ARRAYSIZE, h_result , s_result))
       printf("True\n");
    else
       printf("False\n");
 
    free(h_array);
    free(h_mask);
    free(h_result);
    free(s_result);
    cudaFree(d_array);
    cudaFree(d_mask);
    cudaFree(d_result);  
    return 0;
}
