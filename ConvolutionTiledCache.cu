/********************************************
One dimensional vector colvolution calculation using Tiles and caching in Cuda
 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <cooperative_groups.h>
#include <stdbool.h>

#define BLOCKSIZE   (32)
#define ARRAYSIZE   (256*256)
#define MASKSIZE    (64)

__constant__ int mask [MASKSIZE];

__global__ void ConvolutionTiledCache (int N, int * a, int * c){
  
    int globalIdx = blockIdx.x * blockDim.x + threadIdx.x;
    int convoSum; 

    extern __shared__ int sharedArray[];

    sharedArray[threadIdx.x] = a[globalIdx];
    
    __syncthreads();

    convoSum =0;
    for (int j =  0; j < MASKSIZE ; j++)
         if (threadIdx.x  + j >= blockDim.x  )
             convoSum += a [globalIdx + j] * mask [j]; 
         else    
             convoSum += sharedArray [threadIdx.x  + j ] * mask [j]; 

    c[globalIdx] = convoSum; 
    
    /******************************
    if(  globalIdx >= M/2  &&  globalIdx <  N - M/2) 
       for (int j =  0; j < M ; j++)
           convoSum += a [globalIdx  + j ] * mask [j]; 
    c[globalIdx] = convoSum; 
    *****************************/  
}

void sequential_convolution (int N, int M, int * a, int * b, int * c){
    int convoSum; 
   
    for (int i = 0; i < N ; i++){
        convoSum = 0;
           for (int j = 0; j < M ; j++)
               convoSum += a [i + j ] * b [j];
         
        c[i] = convoSum;         
    }
}

bool comapreResults(int N, int *a , int *b){
    
    for (int i=0; i<N; i++)
      if (a[i] != b [i])
         return false;
   return true;       
}

int main(){
    
    int *h_array , *h_mask, *h_result,  *s_result;
    int *d_array , *d_mask, *d_result ;
    
    float time;
    cudaEvent_t start, stop;
    cudaError_t cudaStat;

    int gridSize = (ARRAYSIZE + BLOCKSIZE - 1) / BLOCKSIZE; 

    printf("\nVector size: %d, Mask size: %d, Block size: %d, Grid size: %d \n", ARRAYSIZE, MASKSIZE, BLOCKSIZE, gridSize);

    h_array = (int*) malloc ( (ARRAYSIZE + 2*(MASKSIZE/2)) * sizeof(int) );
    h_mask  = (int*) malloc ( MASKSIZE  * sizeof(int) );
    h_result= (int*) malloc ( (ARRAYSIZE ) * sizeof(int) );
    s_result= (int*) malloc ( (ARRAYSIZE ) * sizeof(int) );

    for (int i=0; i< (ARRAYSIZE + 2*(MASKSIZE/2)); i++)
       if (i >= MASKSIZE/2 && i < ARRAYSIZE + MASKSIZE/2 ) 
           h_array[i] =  rand()% 10; // ARRAYSIZE;
       else
           h_array[i] = 0;

    for (int i=0; i<MASKSIZE; i++)
       h_mask[i] =  rand()% 10; // ARRAYSIZE;


    printf("\nVector : \n");
    for (int i=0; i < (ARRAYSIZE + 2*(MASKSIZE/2) ) ; i ++)
        printf("%d ", h_array[i]);
    printf("\n");

    printf("\nMask : \n");
    for (int i=0; i< MASKSIZE; i ++)
        printf("%d ", h_mask[i]);
    printf("\n");    

    sequential_convolution (ARRAYSIZE , MASKSIZE , h_array , h_mask, s_result);

    printf("\nSequential result : \n");
    for (int i=0; i< (ARRAYSIZE); i ++)
        printf("%d ", s_result[i]);
    printf("\n");    
   
    cudaStat = cudaMalloc ( (void **) & d_array,  (ARRAYSIZE + 2*(MASKSIZE/2)) * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_mask,  MASKSIZE  *  sizeof (int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       cudaFree(d_array);
       return EXIT_FAILURE;
    }

    cudaStat = cudaMalloc ( (void **) & d_result,  (ARRAYSIZE ) * sizeof(int) );
    if (cudaStat != cudaSuccess) {
       printf ("Cuda Error : %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
       free(h_array);
       free(h_mask);
       free(h_result);
       free(s_result);
       cudaFree(d_array);
       cudaFree(d_mask);
       return EXIT_FAILURE;
    }

    cudaMemcpy ( d_array , h_array,   (ARRAYSIZE + 2*(MASKSIZE/2)) * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol (mask, h_mask, MASKSIZE  * sizeof(int) );
      
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    ConvolutionTiledCache <<<gridSize, BLOCKSIZE, ( BLOCKSIZE + 2*(MASKSIZE/2) ) * sizeof(int) >>>(ARRAYSIZE , d_array, d_result );
    cudaStat  = cudaGetLastError();
    if (cudaStat != cudaSuccess){
          printf("Kernel execution Error: %s at File: %s, Line: %d\n", cudaGetErrorString(cudaStat), __FILE__,  __LINE__);
          free(h_array);
          free(h_mask);
          free(h_result);
          free(s_result);
          cudaFree(d_array);
          cudaFree(d_mask); 
          cudaFree(d_result);       
          return EXIT_FAILURE;
    }
    cudaDeviceSynchronize();

    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel run Time: %.3f ms\n", time);
         
    cudaStat = cudaMemcpy(h_result, d_result, (ARRAYSIZE ) * sizeof(int) , cudaMemcpyDeviceToHost);
    
    printf("\nParallel result : \n");
    for (int i=0; i< (ARRAYSIZE ); i ++)
        printf("%d ", h_result[i]);
    printf("\n");    
 
    printf("\nVerify result: ");
    if (comapreResults((ARRAYSIZE ), h_result , s_result))
       printf("True\n");
    else
       printf("False\n");
 
    free(h_array);
    free(h_mask);
    free(h_result);
    free(s_result);
    cudaFree(d_array);
    cudaFree(d_mask);
    cudaFree(d_result);  
    return 0;
}
