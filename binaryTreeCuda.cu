/********************************************
Implementing binary tree in Cuda 
Author: Maruf Ahmed
mahm1846@uni.sydney.edu.au
********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <iostream> 
#include <algorithm>

#define BLOCKSIZE  (4)
#define GRIDSIZE   (4)
#define ARRAYSIZE  (16) 


static void HandleError( cudaError_t cudaStat, const char *file, int line ) {
  if (cudaStat != cudaSuccess) {
     printf ("Cuda Error : %s in File: %s, at Line: %d\n", cudaGetErrorString(cudaStat), file, line );
     exit( EXIT_FAILURE );
  }
}
#define HANDLE_ERROR( cudaStat ) (HandleError( cudaStat, __FILE__, __LINE__ ))


__global__ void binaryTreeCuda (int N,  int  * a,  int * c,  int * r){
     
    long  globalThreadIdx =  blockIdx.x * blockDim.x +  threadIdx.x;
    int offset; 
    bool newItem = true;
    int rootVal = *r;
    int tempR;
    int cDirection;
    
    offset = 0;
       
    while (globalThreadIdx + offset < N){
         int cIndex;
         if (newItem) {
             newItem = false;
             tempR   = 0;
             cDirection = a[globalThreadIdx + offset] > rootVal ? 1 : 0;
         }
         cIndex = c[tempR*2 + cDirection];
         while (cIndex >= 0){
             tempR = cIndex;
             cDirection = a[globalThreadIdx + offset] > tempR  ? 1 : 0;
             cIndex = c[tempR*2 + cDirection];
         }

         if(cIndex != -2){
             int lockIndex = tempR*2 + cDirection;
             if (atomicCAS ( &c[lockIndex] , cIndex , -2 ) == cIndex){
                 if (cIndex == -1)
                     c[lockIndex] =  a[globalThreadIdx + offset] ;
                  offset +=  gridDim.x  * blockDim.x;
                  newItem = true;
             }
         }
         __syncthreads();
    }
     
}


int main(){
    
    int * h_a , * h_c, * h_r ;
    int * d_a , * d_c, * d_r ; 
  
    float time;
    cudaEvent_t start, stop;

    printf("\nVector size: %d, Block size: %d, grid size: %d \n", ARRAYSIZE, BLOCKSIZE, GRIDSIZE);

    h_a = (int*) malloc ( ARRAYSIZE * sizeof(int));
    h_c = (int*) malloc ( 2*(ARRAYSIZE +1) * sizeof(int));
    h_r = (int*) malloc ( sizeof(int));
 
    for (long i=0; i < ARRAYSIZE; i++)
       h_a[i] = i+1;

    for (long i=0; i < 2*(ARRAYSIZE +1); i++)
       h_c[i] = -1;   
   
    std::random_shuffle(h_a, h_a + ARRAYSIZE);
    /**********************************************/
    /**********      Test values    **************** 
    h_a[0] = 8; h_a[1] = 3; h_a[2] = 12; h_a[3] = 1; 
    h_a[4] = 10; h_a[5] = 5; h_a[6] = 13; h_a[7] = 11; 
    h_a[8] = 6; h_a[9] = 7; h_a[10] = 9; h_a[11] = 14; 
    h_a[12] = 15; h_a[13] = 4; h_a[14] = 16; h_a[15] = 2; 
    /**********************************************/ 

    *h_r =  h_a[0];

    printf("\nVector A: \n");
    for (long i=0; i< ARRAYSIZE; i ++)
        printf("%d ", h_a[i]);
    printf("\n");  
    
    HANDLE_ERROR( cudaMalloc ( (void **) & d_a,  ARRAYSIZE * sizeof(int) ) );
    HANDLE_ERROR( cudaMalloc ( (void **) & d_c,  2*(ARRAYSIZE +1)  * sizeof(int) ) );
    HANDLE_ERROR( cudaMalloc ( (void **) & d_r,  sizeof(int) ) );
 
    cudaMemcpy ( d_a , h_a,  ARRAYSIZE * sizeof(int), cudaMemcpyHostToDevice);
    HANDLE_ERROR( cudaGetLastError() );
    cudaMemcpy ( d_c , h_c,  2*(ARRAYSIZE +1) * sizeof(int), cudaMemcpyHostToDevice);
    HANDLE_ERROR( cudaGetLastError() );
    cudaMemcpy ( d_r , h_r, sizeof(int) , cudaMemcpyHostToDevice);
    HANDLE_ERROR( cudaGetLastError() );
    
    cudaEventCreate(&start);
    cudaEventCreate(&stop); 
    cudaEventRecord (start, 0);

    binaryTreeCuda <<<GRIDSIZE, BLOCKSIZE >>>(ARRAYSIZE, d_a, d_c,  d_r) ;
    HANDLE_ERROR( cudaGetLastError() );
    cudaDeviceSynchronize();
  
    cudaEventRecord(stop, 0);
    cudaEventSynchronize (stop);
    cudaEventElapsedTime (&time, start, stop);
 
    printf("\nKernel Time: %.3f ms\n", time);
     
    cudaMemcpy( h_c, d_c, 2*(ARRAYSIZE +1)  * sizeof(int), cudaMemcpyDeviceToHost) ;
    HANDLE_ERROR( cudaGetLastError() );

    printf("\nVector C: \n");
    for (long i=0; i< 2*(ARRAYSIZE +1); i ++)
        printf("%d ", h_c[i]);
    printf("\n");  

    free(h_a);
    free(h_c);
    free(h_r);
    
    cudaFree(d_a);
    cudaFree(d_c);
    cudaFree(d_r);
    return 0;
}
